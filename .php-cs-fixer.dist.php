<?php

$finder = (new PhpCsFixer\Finder())
    ->in(__DIR__)
    ->exclude('src/Infra/Framework/Symfony/var')
;

return (new PhpCsFixer\Config())
    ->setRules([
        '@Symfony' => true,
    ])
    ->setFinder($finder)
;
