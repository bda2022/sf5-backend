# Procject boileplate for Symfony v5.4 (clean architecture)
This is the backend repository for the Symfony v5.4 application.

## Prerequisites
- PHP 7.4 or higher
- PostgreSQL 12 or higher
- Composer

## Installation
1. Fork this repository and clone it to your local machine
2. Install dependencies by running `composer install` in the terminal
3. Configure database information in `.env`
4. Run migrations with the command `php bin/console doctrine:migrations:migrate`
5. Start the local server with `php bin/console server:run`

## Endpoints
Endpoints will be described soon.

## Contribution
Contributions are welcome! Please open an issue or submit a pull request.

## License
This project is licensed under the MIT License. See the [LICENSE](LICENSE) file for details.
