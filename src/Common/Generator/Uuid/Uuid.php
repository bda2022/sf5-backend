<?php

namespace Common\Generator\Uuid;

abstract class Uuid
{
    /**
     * @throws \Exception
     */
    public static function generate()
    {
        $data = random_bytes(16);
        assert(16 === strlen($data));

        $data[6] = chr(ord($data[6]) & 0x0F | 0x40);
        $data[8] = chr(ord($data[8]) & 0x3F | 0x80);

        return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
    }
}
