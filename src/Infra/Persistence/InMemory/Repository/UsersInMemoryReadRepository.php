<?php

namespace Infra\Persistence\InMemory\Repository;

class UsersInMemoryReadRepository
{
    private array $users;

    public function __construct($users = [])
    {
        $this->users = $users;
    }

    public function findAll(): array
    {
        return $this->users;
    }

    public function findBy(array $criteria): array
    {
        $result = [];
        foreach ($criteria as $key => $value) {
            if (!empty($value)) {
                $result[] = array_search($value, array_column($this->users, $key));
            }
        }

        return $result;
    }

    public function findById(string $id)
    {
        // TODO :
    }
}
