<?php

namespace Infra\Persistence\InMemory\Repository;

class UsersInMemoryWriteRepository
{
    public function __construct(
        private readonly array $users = []
    ) {
    }

    public function create($createUserDto)
    {
        $this->users[] = $createUserDto;
    }

    public function update($createUserDto)
    {
        // TODO: Implement update() method.
    }

    public function delete(string $userId)
    {
        // TODO: Implement delete() method.
    }

    public function findAll(): array
    {
        return $this->users;
    }
}
