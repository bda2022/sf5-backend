<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class AuthController extends AbstractController
{
    #[Route(name: 'auth_index', methods: 'GET', path: '/auth')]
    public function index(): JsonResponse
    {
        return $this->json(['message' => 'Hello world!']);
    }
}
